package com.example.adeyinfa.svgdemo;


import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;


/**
 * MainActivity will handle the creation of the webView and set the image in the link provided
 * below to act as the WebView content.
 */
public class MainActivity extends AppCompatActivity {

    // create an instance of the DBUtil class object
    DBUtil db = new DBUtil();

    WebView webView;
    String start = "G207";
    String dest = "G407";
    // aforementioned link
    final String linkUsed = "http://adepolo.us-west-2.elasticbeanstalk.com/map/index";


    // set the required Api to the latest version (for case of testing to ensure proper
    // functionality.
    @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.N)

    // OnCreate() set the content view to the layout and finds the webview, sets Javascript to be
    // enabled, as well as the display/built-in zoom controls needed to ensure user interaction.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        webView = findViewById(R.id.webView);
        webView.setInitialScale(1);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        // load the url in the webview container
        webView.loadUrl(linkUsed);

        // call upon the map method with the DBUtil class to map the destination from one class
        // to another
        db.map(start,dest);

    }

}

