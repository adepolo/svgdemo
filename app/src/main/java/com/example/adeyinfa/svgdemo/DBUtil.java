package com.example.adeyinfa.svgdemo;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 * DBUtil is a custom class that contains one method, map(String,String), that will handle
 * the post request to the Adepolo server, containing the source and destination rooms. The response
 * should be the path returned from the source room to the destination room. */

public class DBUtil {
    private static final String TAG = "SVGDemo";
    public void map(String from, String to){
        try{
            RequestParams params = new RequestParams();
            params.put("start",from);
            params.put("dest", to);

            AsyncHttpClient client = new AsyncHttpClient();
            client.post("http://adepolo.us-west-2.elasticbeanstalk.com/map/path/",
                    params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int i, cz.msebera.android.httpclient.Header[]
                        headers, byte[] responseBody) {
                    // handle your response from the server here
                    String b =  new String(responseBody);
                    Log.d(TAG,"Success " + b);
                }

                @Override
                public void onFailure(int i, cz.msebera.android.httpclient.Header[] headers,
                                      byte[] responseBody, Throwable throwable) {
                    Log.e(TAG,"Error " + new String(responseBody));
                }

            });
        }catch(Exception ex){
            Log.d(TAG, ex.getMessage());
        } finally {

        }
    }
}
